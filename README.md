# Identity Lib

This Python package contains utilities enabling nicer error descriptions in
OpenAPI specs generated by FastAPI.

## Use

Install `fastapi-openapierrors` using pip:

```
pip install fastapi-openapierrors
```

The module can then be used as `fastapi_openapierrors` to annotate routes:

```python3
from fastapi import FastAPI
from fastapi_openapierrors import NotFoundResponse, BadRequestResponse
from pydantic import BaseModel

app = FastAPI()

class Widget(BaseModel):
    name: str

@api.get(
    '/widget/{name}',
    summary="Get a single widget",
    description="Get a single widget by name",
    response_model=Widget,
    responses={400: BadRequestResponse, 404: NotFoundResponse}
)
async def get_widget(
      name: str = Path(
        ...,
        example=f"frobnicator2000",
        title="The name of the widget"
      )):
    # FastAPI automatically returns a Bad Request response if the request does
    # not match the route.

    # Look for widget by name.
    widget, was_found = get_widget_by_name(name)

    if not was_found:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    return widget
```

## Developer quickstart

This project contains a dockerized testing environment which wraps [tox](https://tox.readthedocs.io/en/latest/).

Tests can be run using the `./test.sh` command:

```bash
# Run all PyTest tests and Flake8 checks
$ ./test.sh

# Run just PyTest
$ ./test.sh -e py3

# Run a single test file within PyTest
$ ./test.sh -e py3 -- tests/test_identifiers.py

# Run a single test file within PyTest with verbose logging
$ ./test.sh -e py3 -- tests/test_identifiers.py -vvv
```
